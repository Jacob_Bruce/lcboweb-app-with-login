package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName(String loginName) {

		boolean validLoginName = true;

		if (loginName.length() < 6) {
			validLoginName = false;
		}
		if (!loginName.matches("[a-zA-Z0-9]*")) {
			validLoginName = false;
		}

		return validLoginName;
	}
}
