package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginCharactersRegular() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("JacobBruce"));
	}

	@Test
	public void testIsValidLoginCharactersException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("!@#$%^&*&^"));
	}

	@Test
	public void testIsValidLoginCharactersBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("Jacob1234"));
	}

	@Test
	public void testIsValidLoginCharactersBoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("JacobBruce!"));
	}

	@Test
	public void testIsValidLoginLengthRegular() {
		assertTrue("Login is not long enough", LoginValidator.isValidLoginName("123456789"));
	}

	@Test
	public void testIsValidLoginLengthException() {
		assertFalse("Login is not long enough", LoginValidator.isValidLoginName("        "));
	}

	@Test
	public void testIsValidLoginLengthBoundaryIn() {
		assertTrue("Login is not long enough", LoginValidator.isValidLoginName("123456"));
	}

	@Test
	public void testIsValidLoginLengthBoundaryOut() {
		assertFalse("Login is not long enough", LoginValidator.isValidLoginName("12345"));
	}

}